function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  sessionStorage.setItem("json", JSON.stringify(objeto));
}
function leerEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function quitarEnSessionStorage()  {
var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
var clave = txtClave.value;
var valor = sessionStorage.getItem(clave);
var spanValor = document.getElementById("spanValor");
sessionStorage.removeItem(clave,valor);
spanValor.innerText= "Clave eliminada: " + clave;

}

function limpiarEnSessionStorage()  {
sessionStorage.clear();
var spanValor = document.getElementById("spanValor"); 
spanValor.innerText= "Se eliminaron todos los datos de la sesión"; 
}

function contarEnSessionStorage(){
var numEle= sessionStorage.length;
var spanValor= document.getElementById("spanValor");
spanValor.innerText = "Se tienen " + numEle + " elementos";
}
